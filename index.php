<?php

require_once 'dal/DAL.php';
require_once 'vendor/tpl.php';
require_once 'dtos/Book.php';
require_once 'dtos/Author.php';
require_once 'Request.php';
require_once 'common.php';

$request = new Request($_REQUEST);
$dal = new DAL();

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'book-list';
$message = $request->param('message')
    ? $request->param('message')
    : '';

$id = $request->param('id');

$messages = [];
if ($message) {
    $messages[] = $message;
}

if ($cmd == 'book-form') {
    $book = null;
    $isEditForm = false;
    if ($id) {
        $book = $dal->getBookById($id);
        $isEditForm = true;
    }

    $data = [
        'book' => $book,
        'isEditForm' => $isEditForm,
        'authors' => $dal->getAuthors(),
        'contentPath' => 'bookForm.html'
    ];
    print renderTemplate('tpl/main.html', $data);

}
else if ($cmd == 'update-book') {

    $book = new Book(
        $request->param('id'),
        $request->param('title'),
        $request->param('grade'),
        intval($request->param('isRead') == 'on')
    );

    if ($request->param('deleteButton')) {
        $dal->deleteBook($book->id);
        redirect('?cmd=book-list&message=Kustutatud');
    }
    else {
        $book->addAuthor($dal->getAuthorById($request->param('author1')));
        $book->addAuthor($dal->getAuthorById($request->param('author2')));

        $errors = validateBook($book);

        if (empty($errors)) {
            $update = '';
            if ($book->id) {
                $dal->updateBook($book);
                $update = 'Muudetud';
            }
            else {
                $dal->addBook($book);
                $update = 'Lisatud';
            }
            redirect('?cmd=book-list&message=' . $update);
        }
        else {
            $data = [
                'contentPath' => 'bookForm.html',
                'authors' => $dal->getAuthors(),
                'book' => $book,
                'errors' => $errors
            ];

            print(renderTemplate('tpl/main.html', $data));
        }
    }
}
else if ($cmd == 'author-list') {
    $data = [
        'messages' => $messages,
        'authors' => $dal->getAuthors(),
        'contentPath' => 'authorList.html'
    ];
    print renderTemplate('tpl/main.html', $data);

}
else if ($cmd == 'author-form') {
    $author = null;
    $isEditForm = false;
    if ($id) {
        $author = $dal->getAuthorById($id);
        $isEditForm = true;
    }

    $data = [
        'contentPath' => 'authorForm.html',
        'author' => $author,
        'isEditForm' => $isEditForm
    ];
    print renderTemplate('tpl/main.html', $data);

}
else if ($cmd == 'update-author') {
    $author = new Author(
        $request->param('id'),
        $request->param('firstName'),
        $request->param('lastName'),
        $request->param('grade')
    );

    if ($request->param('deleteButton')){
        $dal->deleteAuthor($author->id);
        redirect('?cmd=author-list&message=Kustutatud');
    }
    else {
        $errors = validateAuthor($author);

        if (empty($errors)) {
            $update = '';
            if ($author->id) {
                $dal->updateAuthor($author);
                $update = 'Muudetud';
            }
            else {
                $dal->addAuthor($author);
                $update = 'Lisatud';
            }
            redirect('?cmd=author-list&message=' . $update);
        }
        else {
            $data = [
                'contentPath' => 'authorForm.html',
                'author' => $author,
                'errors' => $errors
            ];

            print(renderTemplate('tpl/main.html', $data));
        }
    }
}
else { //if cmd == bookList or other unknown
    if ($cmd != 'book-list') {
        $messages[] = 'Did not find page!';
    }
    $data = [
        'messages' => $messages,
        'books' => $dal->getBooks(),
        'contentPath' => 'bookList.html'
    ];
    print renderTemplate('tpl/main.html', $data);
}


