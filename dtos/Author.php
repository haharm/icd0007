<?php


class Author
{
    public string $id;
    public string $firstName;
    public string $lastName;
    public string $grade;

    function __construct($id, $firstName, $lastName, $grade) {
        $this -> id = $id;
        $this -> firstName = $firstName;
        $this -> lastName = $lastName;
        $this -> grade = $grade;
    }

    public function getFullNameString(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }


    public function __toString(): string
    {
        return $this->id . ' firstName: ' . $this->firstName . ' lastName: ' . $this->lastName;
    }
}