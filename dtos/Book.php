<?php


class Book
{
    public string $id;
    public string $title;
    public string $grade;
    public int $isRead;
    public array $authors = [];

    function __construct(string $id, string $title, string $grade, int $isRead) {
        $this -> id = $id;
        $this -> title = $title;
        $this -> grade = $grade;
        $this -> isRead = $isRead;
    }

    function addAuthor(?Author $author):void
    {
        $this->authors[] = $author;
    }

    function addAuthors(array $authors): void
    {
        foreach ($authors as $author) {
            $this->authors[] = $author;
        }
    }

    public function getAuthorsString(): string
    {
        $names = [];
        foreach ($this->authors as $author) {
            $names[] = $author->firstName . ' ' . $author->lastName;
        }
        return implode(', ', $names);
    }

    public function __toString(): string
    {
        return $this->id . " " . $this->title;
    }
}