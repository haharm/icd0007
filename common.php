<?php

require_once 'dtos/Book.php';
require_once 'dtos/Author.php';

function getAuthorsString(array $authors): string
{
    $res = [];
    foreach ($authors as $author) {
        $res[] = $author->getFirstName() . " " . $author->getLastName();
    }

    return implode(", ", $res);
}

function validateBook(Book $book): array
{
    $errors = [];
    if (strlen($book->title) < 3 || strlen($book->title) > 23) {
        $errors[] = "Pealkirja pikkus peab olema suurem kui 3 märki ja väiksem kui 24 märki!";
    }
    return $errors;
}

function validateAuthor(Author $author): array
{
    $errors = [];
    if (strlen($author->firstName) < 1 || strlen($author->firstName) > 21) {
        $errors[] = "Eesnime lahter ei tohi olla tühi ja eesnime pikkus peab olema max 21 märki!";
    }
    if (strlen($author->lastName) < 2 || strlen($author->lastName) > 22) {
        $errors[] = "Perenime pikkus peab olema suurem kui 2 märki ja väiksem kui 23 märki!";
    }
    return $errors;
}

function redirect($url, $statusCode = 302)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}