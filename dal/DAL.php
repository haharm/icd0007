<?php
require_once './dtos/Author.php';
require_once './dtos/Book.php';


class DAL
{
    private PDO $conn;
    private static string $host = "db.mkalmo.xyz";
    private static string $user = 'haharm';
    private static string $pass = 'f831';
    private static string $database = 'haharm';

    private array $authors;
    private array $books;

    public function __construct() {
        $this->conn = $this->getConnection();
    }

    public function getAuthors(): array
    {
        $this->readAuthors();
        return $this->authors;
    }

    public function getBooks(): array
    {
        $this->readBooks();
        return $this->books;
    }

    private function readAuthors(): void
    {
        $stmt = $this->conn -> prepare("SELECT author_id, firstName, lastName, grade FROM authors");
        $stmt -> execute();

        foreach ($stmt as list($authorId, $firstName, $lastName, $grade)) {
            $this->authors[] = new Author($authorId, $firstName, $lastName, $grade);
        }
    }

    private function readBooks(): void
    {
        $stmt = $this->conn -> prepare(
            "select books.book_id, title, books.grade, isRead, a.author_id, firstName, lastName, a.grade 
                    from books left join author_in_book aib inner join authors a
                        on aib.author_id = a.author_id on books.book_id = aib.book_id;");

        //returns: bookId, title, bookGrade, isRead, author_id, firstName, lastName, authorGrade;
        $stmt -> execute();

        $lastBook = null;
        foreach ($stmt as list($bookId, $title, $bookGrade, $isRead, $authorId, $firstName, $lastName, $authorGrade)) {
            if ($lastBook == null || $lastBook->id != $bookId) {
                $lastBook = new Book($bookId, $title, $bookGrade, $isRead);
                $this->books[] = $lastBook;
            }
            if (isset($authorId, $firstName, $lastName, $authorGrade)){
                $lastBook -> addAuthor(new Author($authorId, $firstName, $lastName, $authorGrade));
            }
        }
    }

    public function getAuthorById($authorId): ?Author
    {
        if (!$authorId) return null;

        $stmt = $this->conn->prepare("select author_id, firstName, lastName, grade from authors where author_id = :id");
        $stmt -> bindValue(":id", $authorId);
        $stmt -> execute();

        foreach ($stmt as list($authorId, $firstName, $lastName, $grade)) {
            return new Author($authorId, $firstName, $lastName, $grade);
        }
        return null;
    }

    public function getBookById($bookID): ?Book
    {
        $stmt = $this->conn -> prepare(
            "select books.book_id, title, books.grade, isRead, a.author_id, firstName, lastName, a.grade 
                    from books left join author_in_book aib inner join authors a
                        on aib.author_id = a.author_id on books.book_id = aib.book_id where books.book_id = :id;");
        $stmt -> bindValue(":id", $bookID);
        $stmt->execute();

        $lastBook = null;
        foreach ($stmt as list($bookId, $title, $bookGrade, $isRead, $authorId, $firstName, $lastName, $authorGrade)) {
            if ($lastBook == null || $lastBook->id != $bookId) {
                $lastBook = new Book($bookId, $title, $bookGrade, $isRead);
            }
            if (isset($authorId, $firstName, $lastName, $authorGrade)){
                $lastBook -> addAuthor(new Author($authorId, $firstName, $lastName, $authorGrade));
            }
        }
        return $lastBook;
    }

    public function addAuthor(Author $author): void
    {
        $stmt = $this->conn -> prepare("insert into authors (firstName, lastName, grade) VALUES
                                                        (:firstName, :lastName, :grade)");
        $stmt -> bindValue(":firstName", $author->firstName);
        $stmt -> bindValue(":lastName", $author->lastName);
        $stmt -> bindValue(":grade", $author->grade);
        $stmt -> execute();
    }

    public function addBook(Book $book): void
    {

        $stmt = $this->conn -> prepare("insert into books (title, grade, isRead) VALUES
                                                        (:title, :grade, :isRead)");
        $stmt -> bindValue(":title", $book->title);
        $stmt -> bindValue(":grade", $book->grade);
        $stmt -> bindValue(":isRead", $book->isRead);
        $stmt -> execute();

        $bookId = $this->conn -> lastInsertId();

        foreach ($book->authors as $author) {
            if (!isset($author)) continue;
            $stmt = $this->conn -> prepare("insert into author_in_book (author_id, book_id) VALUES (:authorId, :bookId)");
            $stmt -> bindValue(":authorId", $author->id);
            $stmt -> bindValue(":bookId", $bookId);
            $stmt -> execute();
        }
    }

    public function deleteAuthor($id): void
    {
        $stmt = $this->conn -> prepare("DELETE from authors where author_id = :id");
        $stmt -> bindValue(":id", $id);
        $stmt -> execute();
    }

    public function deleteBook($id): void
    {
        $stmt = $this->conn -> prepare("DELETE from books where book_id = :id");
        $stmt -> bindValue(":id", $id);
        $stmt -> execute();

        $stmt = $this->conn -> prepare("DELETE from author_in_book where book_id = :id");
        $stmt -> bindValue(":id", $id);
        $stmt -> execute();
    }

    public function updateBook(Book $book): void
    {
        $stmt = $this->conn -> prepare("UPDATE books SET title=:title,
                   grade=:grade, isRead=:isRead WHERE book_id = :id;");
        $stmt -> bindValue(":title", $book->title);
        $stmt -> bindValue(":grade", $book->grade);
        $stmt -> bindValue(":isRead", $book->isRead);
        $stmt -> bindValue(":id", $book->id);
        $stmt -> execute();

        $stmt = $this->conn -> prepare("delete from author_in_book where book_id = :bookId;");
        $stmt -> bindValue(":bookId", $book->id);
        $stmt -> execute();

        foreach ($book->authors as $author) {
            $stmt = $this->conn -> prepare("insert into author_in_book (author_id, book_id) VALUES (:authorId, :bookId)");
            $stmt -> bindValue(":authorId", $author->id);
            $stmt -> bindValue(":bookId", $book->id);
            $stmt -> execute();
        }
    }

    public function updateAuthor(Author $author): void
    {
        $stmt = $this->conn -> prepare("UPDATE authors SET firstName=:firstName,
                   lastName=:lastName, grade=:grade WHERE author_id = :id;");
        $stmt -> bindValue(":firstName", $author->firstName);
        $stmt -> bindValue(":lastName", $author->lastName);
        $stmt -> bindValue(":grade", $author->grade);
        $stmt -> bindValue(":id", $author->id);
        $stmt -> execute();
    }

    private static function getConnection(): PDO
    {
        $address = sprintf('mysql:host=%s;dbname=%s', self::$host, self::$database);

        try {
            return new PDO($address, self::$user, self::$pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            throw new RuntimeException("Can't connect: \n" . $e);
        }
    }
}